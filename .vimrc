if has("autocmd")
  augroup vimrc
    " remove ALL autocommands for the current group.
    :autocmd!
    " source the vimrc file after saving it
    autocmd BufWritePost .vimrc source $MYVIMRC
    " when entering a buffer change the current working directory
    " show marks by default
    "autocmd VimEnter * DoShowMarks!
    " open a scratch buffer automatically when vim starts up if no files were specified
    autocmd StdinReadPre * let s:std_in=1
    autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | setlocal buftype=nofile | setlocal bufhidden=hide | setlocal noswapfile | endif
    " map txt to markdown
    autocmd BufNewFile,BufReadPost *.txt set filetype=text
    autocmd BufNewFile,BufReadPost *.enc set filetype=text
  augroup END
endif

" break vi compatibility. Must be first
set nocompatible
"execute pathogen#infect()

filetype plugin indent on
syntax on

set background=dark
colorscheme zenburn
syntax enable

" leader mappings
let mapleader=" "
"  visual mode
"   format
vnoremap <leader>fj :'<,'>s.{.\r{.g<cr>:'<,'>s., *.,\r.g<cr>
vnoremap <leader>fx :set filetype=xml<cr>:s
"   sort
vnoremap <leader>s :sort<cr>
vnoremap <leader>u :sort u<cr>
"   start system register
vnoremap <leader>r "+
"  normal mode
"   text headers
nnoremap <leader>1 yypVr=
nnoremap <leader>2 yypVr-
nnoremap <leader>3 I###<space><esc>
"   quick edit
nnoremap <leader>ebp :e ~/.bash_profile<cr>
nnoremap <leader>ehe :e ~/.vim/help.txt<cr>
nnoremap <leader>ene :enew<cr>
nnoremap <leader>eno :e ~/OneDrive/Consensus/notes.txt<cr>
nnoremap <leader>esc :enew<cr>:setlocal buftype=nofile bufhidden=hide noswapfile<cr>:echo ""<cr>
nnoremap <leader>ef  :setlocal buftype= bufhidden= swapfile<cr>:echo ""<cr>
nnoremap <leader>est :e ~/OneDrive/Consensus/status.txt<cr>
nnoremap <leader>esy :e ~/.vim/syntax/text.vim<cr>
nnoremap <leader>ev :e $MYVIMRC<cr>
"   format
nnoremap <leader>fj :%s.{.\r{.g<cr>:%s., *.,\r.g<cr>:g/^$/d<cr>gg=G
nnoremap <leader>fx :set filetype=xml<cr>:%s/></>\r</g<cr>:g/^$/d<cr>gg=G
nnoremap <leader>i 0i
"   buffers
nnoremap <leader>h :bn<cr>
nnoremap <leader>j :bp\|bd #<cr>
nnoremap <leader>k :ls<cr>:b
nnoremap <leader>l :bp<cr>
"   windows
nnoremap <leader>H <c-w>h
nnoremap <leader>J <c-w>j
nnoremap <leader>K <c-w>k
nnoremap <leader>L <c-w>l
nnoremap <leader><leader>m :NoShowMarks!<cr>:DoShowMarks!<cr>
nnoremap <leader>m :DoShowMarks!<cr>
nnoremap <leader>M :NoShowMarks!<cr>
"   outline
nnoremap <leader>o1 I? <esc>
nnoremap <leader>o2 I? <esc>
nnoremap <leader>o3 I? <esc>
"   clear highlights
nnoremap <leader>hl :set hlsearch!<cr>
"   NERDTree
nnoremap <leader>nt :NERDTree
"   toggle relative/absolute numbering
nnoremap <leader>nu :set rnu!<cr>
"   start system register
nnoremap <leader>r "+
nnoremap <leader>R o<esc>"+
"   splits
nnoremap <leader>sp :sp<cr><c-w>j:bp<cr>
nnoremap <leader>vsp :vsp<cr><c-w>l:bp<cr>
"    h split resize uses - = for faster - + and _ + for min/max
nnoremap <leader>+ <c-w>_
nnoremap <leader>- <c-w>-
nnoremap <leader>= <c-w>+
nnoremap <leader>_ :resize 1<cr>
"    v split resize uses , . for faster < > and < > for min/max
nnoremap <leader>, <c-w><
nnoremap <leader>. <c-w>>
nnoremap <leader>< :vertical resize 1<cr>
nnoremap <leader>> <c-w>\|
"    [v|h] split equal uses \ since - = is faster for resize
nnoremap <leader>\ <c-w>=
"   tab stops
"nnoremap <leader>t1 :set tabstop=2 softtabstop=2<cr>
"nnoremap <leader>t2 :set tabstop=4 softtabstop=4<cr>
"nnoremap <leader>t3 :set tabstop=8 softtabstop=8<cr>
"nnoremap <leader>t4 :set tabstop=16 softtabstop=16<cr>
"nnoremap <leader>t5 :set tabstop=32 softtabstop=32<cr>
"nnoremap <leader>t6 :set tabstop=64 softtabstop=64<cr>
"nnoremap <leader>t7 :set tabstop=128 softtabstop=128<cr>
"nnoremap <leader>t8 :set tabstop=256 softtabstop=256<cr>
"nnoremap <leader>t9 :set tabstop=512 softtabstop=512<cr>
"nnoremap <leader>t0 :set tabstop=1024 softtabstop=1024<cr>
" trim trailing spaces
nnoremap <leader>tt :%s/\s\+$//<cr><c-o>
"   wrap
nnoremap <leader>w :setlocal wrap!<cr>zz
"   spell check
nnoremap <leader>zz :setlocal spell!<cr>
"    change to first suggestion
nnoremap <leader>z= z=1<cr><cr>
"    mark word good
nnoremap <leader>zg zg
"   move between tab columns
nnoremap <leader><tab> f<tab><right>
nnoremap <leader><s-tab> F<tab>;<right>

" non-leader mappings
"  visual mode
"   enter starts command mode
vnoremap <cr> :
"  insert mode
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
inoremap <up> <nop>
"inoremap jj <Esc>
"  normal mode
"   back space deletes
nnoremap <bs> dh
"   enter starts command mode
nnoremap <cr> :
nnoremap $ g$
nnoremap 0 g0
nnoremap j gj
nnoremap k gk
"   echo paste status when changed
nnoremap <f2> :set invpaste paste?<cr>
"   match pairs with tab
nnoremap <tab> %
"   join lines but stay in place
nnoremap J mzJ`z
"   next then center vertically
nnoremap N Nzz
nnoremap n nzz
"   make Y behave like D C, etc
nnoremap Y y$
"   move to mark then center vertically
nnoremap 'a 'azz
nnoremap 'b 'bzz
nnoremap 'c 'czz
nnoremap 'd 'dzz
nnoremap 'e 'ezz
nnoremap 'f 'fzz
nnoremap 'g 'gzz
nnoremap 'h 'hzz
nnoremap 'i 'izz
"nnoremap 'j 'jzz
nnoremap 'k 'kzz
nnoremap 'l 'lzz
nnoremap 'm 'mzz
nnoremap 'n 'nzz
nnoremap 'o 'ozz
nnoremap 'p 'pzz
nnoremap 'q 'qzz
nnoremap 'r 'rzz
nnoremap 's 'szz
nnoremap 't 'tzz
nnoremap 'u 'uzz
nnoremap 'v 'vzz
nnoremap 'w 'wzz
nnoremap 'x 'xzz
nnoremap 'y 'yzz
nnoremap 'z 'zzz
"   visual mode
vnoremap < <gv
vnoremap > >gv
vnoremap <bs> d
"   match pairs with tab
vnoremap <tab> %

"set autochdir " tried autocmd BufEnter * silent! lcd %:p:h with no success
set autoindent
set backspace=indent,start
" use the system clipboard
set clipboard=unnamed
" copy the previous indentation on autoindenting
set copyindent
set cursorline
set encoding=utf-8
" hide buffers do not force save/replace
set hidden
" highlight search hits
set hlsearch
" case insensitive searches. see smartcase
set ignorecase
" start showing results as you type
set incsearch
let $LANG='en_US.utf-8'
set spelllang=en_us
" two lines so that staus does not get wiped with --Insert--
set laststatus=2
" turn on list-hidden-chars-mode. see listchars
set linebreak nolist " when wrap is on wrap on space only
set list
" other listchars 215:× 247:÷ 187:» 183:· 172:¬
set listchars=tab:\|\ ,trail:.,extends:#,nbsp:.
set matchpairs=(:),{:},[:],<:>
" stop vi droppings
set nobackup
" do not beep
set noerrorbells
" <tab> means tab
"set noexpandtab
" do not wrap lines
set nowrap
" do not wrap searches
set nowrapscan
" show line numbers
set number
set numberwidth=3
" allows pastetoggle in insert mode
set pastetoggle=<f2>
set ruler
set showcmd
set showmode
" show matching parenthesis
set showmatch
" start scrolling n characters from right edge
set sidescrolloff=10
" ignore case if search is all lower
set smartcase
"set statusline=
"set statusline+=[%{strlen(&fenc)?&fenc:'none'}, " file encoding
"set statusline+=%{&ff}]    " file format
"set statusline+=%h         " help file flag
"set statusline+=%y         " filetype
"set statusline+=%r         " read only flag
"set statusline+=\ \ \ \ %F " filename
"set statusline+=%4m        " modified flag
"set statusline+=%=         " left/right separator
"set statusline+=%c:        " cursor column
"set statusline+=%l/%L      " cursor line/total lines
"set statusline+=\ %P       " percent through file
set tabstop=2 expandtab softtabstop=0 shiftwidth=2 smarttab
" disabled to be consistent with Eclipse and IntelliJ plugins
" set tildeop" change ~ to take movement
" change the terminal's title
set title
" do not beep
set visualbell
" ignore some extensions with auto-complete file names
set wildignore=*.swp,*.class,*.bak
" tab completion selection
set wildmenu
" set wildmode=list:longest,full

" variables
let showmarks_include = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
let g:airline_theme='zenburn'

if has("gui_running")
  " GVIM only options
  colorscheme zenburn
  " hide toolbar
  set guioptions-=T
  " selecting text with the mouse will copy
  set guioptions+=a
  " since we do not wrap lines, show the horizontal scrollbar
  set guioptions+=b
  set lines=42 columns=156
  set sessionoptions+=resize,winpos
  winpos 440 0
  if has("win32")
    set guifont=Consolas:h11:cANSI
  else
    set guifont=Osaka-Mono:h22
  endif
  set diffexpr=MyDiff()
  function! MyDiff()
    let opt = '-a --binary '
    if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
    if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
    let arg1 = v:fname_in
    if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
    let arg2 = v:fname_new
    if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
    let arg3 = v:fname_out
    if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
    let eq = ''
    if $VIMRUNTIME =~ ' '
      if &sh =~ '\<cmd'
        let cmd = '""' . $VIMRUNTIME . '\diff"'
        let eq = '"'
      else
        let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
      endif
    else
      let cmd = $VIMRUNTIME . '\diff'
    endif
    silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
  endfunction
else
  " MinTTY only options see: https://code.google.com/p/mintty/wiki/Tips
  "mode dependent cursor
  let &t_EI.="\e[1 q"
  let &t_SI.="\e[5 q"
  let &t_te.="\e[0 q"
  let &t_ti.="\e[1 q"
  " fix escape timeout
  let &t_te.="\e[?7727l"
  "  let &t_ti.="\e[?7727h" causes z<esc> to O and [
  noremap <esc>O[ <esc>
  noremap! <esc>O[ <esc>
endif
