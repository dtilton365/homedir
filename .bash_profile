# OS X doesn't read .bashrc file on bash start.
# Instead, it reads the following files (in the following order):
# /etc/profile
# ~/.bash_profile
# ~/.bash_login
# ~/.profile

# include bashrc from DevHome repository
. ~/.bashrc

export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=10000

export PAM_WORKDIR=~/WorkingDir/PAM

function mkdir-() { mkdir -p "$@" && eval cd "\"\$$#\""; }

alias cd2='cd ../..'
alias cd3='cd ../../..'
alias cd4='cd ../../../..'
alias cls='printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";clear'
alias dc='docker-compose'
alias fd='find . -type d'
alias ff='find . -type f'
alias ll='ls -hl'
alias ls='/usr/local/bin/gls --color -h --group-directories-first'
alias vi-notes='~/OneDrive/Consensus/notes.txt'
alias vi='mvim -v'
alias vim='mvim -v'

# recommended by bash-git-prompt
#if [ -f "/usr/local/opt/bash-git-prompt/share/gitprompt.sh" ]; then
  #__GIT_PROMPT_DIR="/usr/local/opt/bash-git-prompt/share"
  #source "/usr/local/opt/bash-git-prompt/share/gitprompt.sh"
#fi

# requied by bash-completion
[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion

LightGreen="$(tput bold ; tput setaf 2)"
NC="$(tput sgr0)" # No Color
export PS1="\[\e]0;\w\a\]\[$LightGreen\]\u@\h:\w\[$NC\]\n> "

# disable ctrl-s for screen pausing so that it will reverse ctrl-r history search
stty -ixon

set -o vi

# locations; at end so we can echo alias >> ...
alias cd-dl='cd ~/Downloads'
alias cd-logs='cd ~/WorkingDir/Docker/logs/'
alias cd-wd='cd ~/WorkingDir'
alias cd-shop='cd ~/WorkingDir/ConnectedCommerceShopping'
